function camelize(str) {
    let strArr = str.split("-");
    let newStr = strArr[0];
    for (let i = 0; i < strArr.length-1; i++) {
        newStr = newStr + strArr[i+1].charAt(0).toUpperCase() +
            strArr[i+1].slice(1);
    }
    return newStr;
}
alert(camelize("list-style-image"));
