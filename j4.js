function filterRangeInPlace(arr, a, b) {
    for(let i = 0; i < arr.length; i++) {
        if(arr[i] < a || arr[i] > b) {
            arr.splice(i, 1);
            i--;
        }
    }
    return arr;
}
let arr1 = [5, 3, 8, 1];
filterRangeInPlace(arr1, 1, 4);
alert( arr1 );