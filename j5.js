function reverseSort(arr){
    arr.sort((a, b) => {
    if(a < b) return 1;
    if(a > b) return -1;
    return 0;
});
return arr;
}
let arr2 = [5, 2, 1, -10, 8];
reverseSort(arr2);
alert(arr2);