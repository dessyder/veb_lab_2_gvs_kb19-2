let list = {
    value: 1,
    next: {
        value: 2,
        next: {
            value: 3,
            next: {
                value: 4,
                next: null
            }
        }
    }
};
function PrintList(list) {
    let elem = list;
    while(elem) {
        console.log(elem.value);
        elem = elem.next;
    }
}
function PrintListRec(list){
    let elem = list;
    console.log(elem.value);
    if(elem.next){
        PrintListRec(elem.next);
    }
}
function PrintReverseListRec(list){
    let elem = list;
    if(elem.next){
        PrintReverseListRec(elem.next)
    }
    console.log(elem.value);
}
function PrintReverseList(list){
    let elem = list, arr = [];
    while(elem) {
        arr.push(elem.value);
        elem = elem.next;
    }
    for (let i = arr.length-1; i >= 0; i--) {
        console.log(arr[i]);
    }
}
PrintList(list);
PrintListRec(list);
PrintReverseListRec(list);
PrintReverseList(list);

