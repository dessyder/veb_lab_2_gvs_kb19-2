function sortRandom(arr) {
    arr.sort(function (a, b) {
        return Math.random() - 0.5;
    });
}
let arr4 = [1, 2, 3, 4, 5];
sortRandom(arr4);
alert(arr4);