
function addClass(obj, cls)
{
    if(!obj.className.includes(cls.trim() + " "))
    {
        obj.className += " " + cls;
        obj.className = obj.className.replace(/ +/g, ' ').trim();
    }
}
let obj = {
    className: 'open menu'
}
addClass(obj, 'open');
addClass(obj, 'me');
addClass(obj, 'new');
alert( obj.className );