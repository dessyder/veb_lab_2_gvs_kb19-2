function unique (arr){
    let unArr = [];
    let j = 0;
    for(let i = 0; i < arr.length; i++) {
        if (!unArr.includes(arr[i])) {
            unArr[j] = arr[i];
            j++;
        }
    }
    return unArr;
}

let strings = ["C++", "C++", "С#", "C", "C++", "JavaScript", "C++", "JavaScript"];
alert(unique(strings));